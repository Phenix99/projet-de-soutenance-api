const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

const EtatSchema = mongoose.Schema({
    nom: {type: String, required: true, unique: true},
    numero: {type: String, required: true, unique: true},
    information: {type: String, required: true, unique: true}
}); 

EtatSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Etat', EtatSchema);