const express = require('express');
const router = express.Router();

const userCtrl = require('../controllers/user');

const authGuard = require('../middlewares/auth');

router.get('/:id', authGuard, userCtrl.read);

router.post('/login',  (req, res, next) => {
    next();
}, userCtrl.login);

router.post('/signup', userCtrl.signupStudent);

router.post('/resp/signup',  (req, res, next) => {
    req.body.role = 'Responsable';
    req.body.validated = false;
    next();
}, userCtrl.signup);
router.post('/admin/signup',  (req, res, next) => {
    req.body.role = 'Administrateur';
    next();
}, userCtrl.signup);

module.exports = router