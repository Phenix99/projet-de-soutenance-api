const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');

const pieceJointeSchema = mongoose.Schema({
    description: {type: String, required: true},
    image_url: {type: String, require: true},
});

module.exports = mongoose.model('PieceJointe', pieceJointeSchema);