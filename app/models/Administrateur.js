const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');
const User = require('./User');

const administrateurSchema = mongoose.Schema({
    
});

administrateurSchema.plugin(uniqueValidator);

module.exports = User.discriminator('Administrateur', administrateurSchema);