const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

const domaineSchema = mongoose.Schema({
    nom: {type: String, required: true, unique: true}
}); 

domaineSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Domaine', domaineSchema);