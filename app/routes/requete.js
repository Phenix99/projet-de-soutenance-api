const express = require('express');
const router = express.Router();

const authGuard = require('../middlewares/auth');

const multerGuard = require('../middlewares/multer-config');

const requeteCtrl = require('../controllers/requete');

router.get('/type', requeteCtrl.readAllTypeRequete);

router.post('/', authGuard, multerGuard.array('piece', 12), requeteCtrl.beforeCreate, requeteCtrl.create);

router.get('/:id', authGuard, requeteCtrl.read);

router.put('/:id', authGuard, multerGuard.array('piece', 12), requeteCtrl.update);

router.delete('/:id', authGuard, requeteCtrl.delete);

router.use('/', /*authGuard,*/ requeteCtrl.readAll);

module.exports = router