const mongoose = require('mongoose');
const Etudiant = require('./Etudiant');
const Schema = mongoose.Schema;

const inscriptionAcademiqueSchema = mongoose.Schema({
    id_etu: {type: Schema.Types.ObjectId, ref: 'Etudiant', required: true},
    id_fil: {type: Schema.Types.ObjectId, ref: 'Filiere', required: true},
    id_niv: {type: Schema.Types.ObjectId, ref: 'Niveau', required: true},
    ues: [{type: Schema.Types.ObjectId, ref: 'UE', required: true}],
    annee_aca: {type: String, required: true}
});

module.exports = mongoose.model('InscriptionAcademique', inscriptionAcademiqueSchema);