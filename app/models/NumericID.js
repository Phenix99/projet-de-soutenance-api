const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');

function getNewNumericID(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

const numericIDSchema = mongoose.Schema({
    num_id: {type: String, default: getNewNumericID(6), required: true, unique: true},
    document_type:  {type: String, required: true},
    id_user: {type: Schema.Types.ObjectId, ref: 'User', required: true},
    id_ins_aca: {type: Schema.Types.ObjectId, ref: 'InscriptionAcademique', required: false},
    created_at: {type: Date, default: Date.now}
});


numericIDSchema.plugin(uniqueValidator);

module.exports = mongoose.model('NumericID', numericIDSchema);