const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

const anneeSchema = mongoose.Schema({
    nom: {type: String, required: true, unique: true}
}); 

anneeSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Annee', anneeSchema);