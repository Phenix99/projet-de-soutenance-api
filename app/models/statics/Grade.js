const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

const GradeSchema = mongoose.Schema({
    nom: {type: String, required: true, unique: true}
}); 

GradeSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Grade', GradeSchema);