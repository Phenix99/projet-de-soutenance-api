const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');
const User = require('./User');

const ExpertSchema = mongoose.Schema({
    idEtab: {type: String, required: true, unique: true}
}); 

ExpertSchema.plugin(uniqueValidator);

module.exports = User.discriminator('Expert', ExpertSchema);
