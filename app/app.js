/* eslint-disable no-unused-vars */
const express = require('express');
const bodyParser = require('body-parser');
var history = require('connect-history-api-fallback');
var path = require('path');

const app = express();

const mongoose = require('mongoose');

const userRoutes = require('./routes/user');
const requeteRoutes = require('./routes/requete');
const inscriptionAcademiqueRoutes = require('./routes/inscription_academique');
const preinscriptionRoutes = require('./routes/preinscription');
const numericIDRoutes = require('./routes/numeric_id');

const ONLINE_DB = 'mongodb+srv://phenixmdb:oU0X8oOjHFLh1Fxo@phenix99-mrv2q.mongodb.net/uy1?retryWrites=true&w=majority';
const OFFLINE_DB = 'mongodb://localhost:27017/uy1';

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
};


mongoose.connect(ONLINE_DB, options)
		.then(() => console.log('Conection à MongoDB réussie !'))
		.catch((error) => console.log('Conection à MongoDB échouée ! \n' + error));

//For Parse Response Body when files has not send
app.use(bodyParser.json());

//For Vue.js Url Hastag remove
app.use(history());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

app.use('/api/auth', userRoutes);
app.use('/api/requete', requeteRoutes);
app.use('/api/check', numericIDRoutes);
app.use('/api/ins_aca', inscriptionAcademiqueRoutes);
app.use('/api/preinscription', preinscriptionRoutes);

app.use(express.static('public'));

module.exports = app;