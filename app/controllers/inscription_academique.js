/* eslint-disable no-unused-vars */
const InscriptionAcademique = require('../models/InscriptionAcademique');
const Filiere = require('../models/Filiere');
const Niveau = require('../models/Niveau');
const Departement = require('../models/Departement');
const UE = require('../models/UE');
const Annee = require('../models/statics/Annee');
const Banque = require('../models/statics/Banque');
const Etudiant = require('../models/Etudiant');
const Recu = require('../models/Recu');
const User = require('../models/User');
const jwt = require('jsonwebtoken');


var today = new Date();
var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
var dateTime = date + ' ' + time;

exports.beforeCreate = (req, res, next) => {
    User.findOne({matricule: req.body.student.matricule})
        .then( user => {
            if(!user){
                req.body.student.password = '';
                req.body.student.email = 'example' + Math.floor(Math.random()*1000000) + date + time + "@mail.com";
                //Si l'Etudiant n'existe pas encore
                console.clear()
                console.log(req.body.student)
                const us = new Etudiant({ ...req.body.student });
                us.save()
                    .then( (usr) => {
                        req.body.student = usr;
                        next()
                    } )
                    .catch( error =>console.log(error) );
            }else{
                //Si l'Etudiant existe déjà ou a déjà fait une inscription académique
                if( user.password === '' ) {
                    User.updateOne({_id: user._id}, {...req.body.student, _id: user._id })
                        .then( (u) =>  {
                             User.findOne({_id: user._id})
                                .then( (us) =>  {
                                req.body.student = us;
                                next()
                            } )
                            .catch( error =>res.status(500).json({ error }) );
                        } )
                        .catch( error =>res.status(500).json({ error }) );
                }else  {

                    try {
                        //console.log(JSON.stringify(req.headers))
                        const token = req.headers.authorization.split(' ')[1];
                        const decodedToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET');
                        const userId = decodedToken.userId;
                        if(req.body.student._id && req.body.student._id !== userId){
                            throw 'Invalid User ID !';
                        } else {
                            console.log(JSON.stringify(req.body));
                            var options = {upsert: true, new: true,};
                            User.findOneAndUpdate({ _id: userId }, { ...req.body.student, _id: userId }, options)
                                .exec()
                                .then( (resu) => {
                                    console.log('Update Handle')
                                    req.body.student = resu;
                                    next();
                                } )
                                .catch( error => res.status(400).json({ error }) );
                        }
                    } catch {
                        res.status(401).json({ 
                            error: "Cet étudiant a déjà un compte.\nVous ne pouvez pas modifier ses données !"
                        });
                    }
                }
            
            }
        })
        .catch( error =>res.status(500).json({ error }) );
};

exports.create = (req, res, next) => {
    if (req.body.annee_aca === "") {req.body.annee_aca = today.getFullYear() + '-' + (today.getFullYear() + 1)}

    InscriptionAcademique.deleteOne({ annee_aca : req.body.annee_aca, id_etu:  req.body.student._id})
        .then( (m) => {console.log("Anciennes Inscription retirées " + m)} )
        .catch( error => res.status(401).json({ error }) );
    
    const inscription_academique = new InscriptionAcademique({ ...req.body, id_etu: req.body.student._id});
    
    inscription_academique.save()
        .then( inscription_academique => {
            req.body.recus.forEach( r => {
                const recu = new Recu({ ...r, id_ins_aca: inscription_academique._id });
                recu.save()
                    .then( () => console.log("1 reçu enregistré ") )
                    .catch( error => res.status(500).json({ error }) );
            });
            res.status(200).json({ inscription_academique })
        } )
        .catch( error => res.status(401).json({ error }) );
}
exports.search = (req, res, next) => {

    var populateQuery = [
        {path:'id_etu', model: 'Etudiant'}, 
        {path:'id_fil', select:'nom', model: 'Filiere'}, 
        {path:'id_niv', select:'nom', model: 'Niveau'}
    ];
    console.log({...req.body})
    InscriptionAcademique.findOne({...req.body})
        .populate(populateQuery)
        .exec(function(err, inscription_academique) {
            res.status(200).json({ inscription_academique})
        } )
}

exports.filter = (req, res, next) => {
    var populateQuery = [
        {path:'id_etu', model: 'Etudiant'},
        {path:'id_fil', select:'nom', model: 'Filiere'},
        {path:'id_niv', select:'nom', model: 'Niveau'},
    ];
    console.log({...req.body})
    InscriptionAcademique.find({...req.body})
        .populate(populateQuery)
        .then(inscriptionAcademiques => res.status(200).json(inscriptionAcademiques))
        .catch(error => res.status(400).json({error}));
}

exports.paginate = (req, res, next) => {
    const pageOptions = {
        page: parseInt(req.query.page, 10) || 0,
        limit: parseInt(req.query.limit, 10) || 10
    };
    
    InscriptionAcademique.find()
        .skip(pageOptions.page * pageOptions.limit)
        .limit(pageOptions.limit)
        .exec(function (err, inscriptionAcademiques) {
            if(err) { 
                res.status(500).json(err); 
                return; 
            };
            res.status(200).json(inscriptionAcademiques);
        });
}

exports.update = (req, res, next) => {
   InscriptionAcademique.updateOne({ _id: req.params.id }, { ...req.body, _id: req.params.id })
        .then( () => res.status(200).json({ message: 'Modified!' }) )
        .catch( error => res.status(400).json({ error }) );
}
exports.delete = (req, res, next) => {
    InscriptionAcademique.deleteOne({_id: req.params.id})
        .then( () => res.status(200).json({ message: 'Deleted!' }) )
        .catch( error => res.status(404).json({ error }) );
}
exports.read = (req, res, next) => {
    InscriptionAcademique.findById(req.params.id)
        .then( inscription_academique => {
            res.status(200).json({ inscription_academique })
        })
        .catch( error => res.status(404).json({ error }) );
}
exports.readAll = (req, res, next) => {
    var populateQuery = [
        {path:'id_etu', model: 'Etudiant'},
        {path:'id_fil', select:'nom', model: 'Filiere'},
        {path:'id_niv', select:'nom', model: 'Niveau'}
    ];
    InscriptionAcademique.find()
        .populate(populateQuery)
        .then(inscription_academiques => {
            res.status(200).json(inscription_academiques)
        })
        .catch(error => {
            res.status(400).json({ error })
        });
}
exports.readAllFiliere = (req, res, next) => {
    Filiere.find()
        .then(filieres => {
            res.status(200).json({ filieres })
        })
        .catch(error => {
            res.status(400).json({ error })
        });
}
exports.readAllUE = (req, res, next) => {
    UE.find()
        .then(ues => {
            res.status(200).json({ ues })
        })
        .catch(error => {
            res.status(400).json({ error })
        });
}
exports.readAllNiveau = (req, res, next) => {
    Niveau.find()
        .then(niveaus => {
            res.status(200).json({ niveaus })
        })
        .catch(error => {
            res.status(400).json({ error })
        });
}
exports.readAllBanque = (req, res, next) => {
    Banque.find()
        .then(banques => {
            res.status(200).json({ banques })
        })
        .catch(error => {
            res.status(400).json({ error })
        });
}
exports.readAllAnnee = (req, res, next) => {
    Annee.find()
        .then(annees => {
            res.status(200).json({ annees })
        })
        .catch(error => {
            res.status(400).json({ error })
        });
}

exports.readAllDepartement = (req, res, next) => {
    Departement.find()
        .then(departements => {
            res.status(200).json({ departements })
        })
        .catch(error => {
            res.status(400).json({ error })
        });
}

exports.readRecuByInsAca = (req, res, next) => {

    Recu.find({id_ins_aca: req.params.id_ins_aca})
        .then(recus => {
            res.status(200).json(recus)
        })
        .catch(error => {
            res.status(400).json({ error })
        });
}