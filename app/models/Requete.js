const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');

const requeteSchema = mongoose.Schema({
    id_fil: {type: Schema.Types.ObjectId, ref: 'Filiere', required: true},
    id_niv: {type: Schema.Types.ObjectId, ref: 'Niveau', required: true},
    id_etu: {type: Schema.Types.ObjectId, ref: 'Etudiant', required: true},
    id_typ_req: {type: Schema.Types.ObjectId, ref: 'TypeRequete', required: true},
    id_eta_req: {type: Schema.Types.ObjectId, ref: 'EtatRequete', default: '5f40cda2e373bb20263bf837', required: true},
    pieces_jointes: [{type: Schema.Types.ObjectId, ref: 'PieceJointe', required: false}],
    date_ajout: {type: Date, default: Date.now},
    description: {type: String, required: true},
    observations: {type: String, required: false}
});


requeteSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Requete', requeteSchema);