const express = require('express');
const router = express.Router();

const inscriptionCtrl = require('../controllers/inscription_academique')

router.get('/filiere', inscriptionCtrl.readAllFiliere);
router.get('/banque', inscriptionCtrl.readAllBanque);
router.get('/annee', inscriptionCtrl.readAllAnnee);
router.get('/ue', inscriptionCtrl.readAllUE);
router.get('/niveau', inscriptionCtrl.readAllNiveau);
router.get('/departement', inscriptionCtrl.readAllDepartement);
router.post('/search', inscriptionCtrl.search);
router.post('/filter', inscriptionCtrl.filter);
router.post('/paginate', inscriptionCtrl.paginate);
router.get('/recu/:id_ins_aca', inscriptionCtrl.readRecuByInsAca);

router.get('/', inscriptionCtrl.readAll);

router.get('/:id', inscriptionCtrl.read);

router.post('/', inscriptionCtrl.beforeCreate, inscriptionCtrl.create);

router.put('/:id', inscriptionCtrl.update);

router.delete('/:id', inscriptionCtrl.delete);

module.exports = router

