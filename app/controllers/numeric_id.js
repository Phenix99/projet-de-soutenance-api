const NumericID = require('../models/NumericID');
const fs = require('fs');

exports.create = (req, res, next) => {

    const numericID = new NumericID({
        ...req.body
    });
    numericID.save()
        .then((id_numeric) => {
            res.status(201).json(id_numeric);
        })
        .catch(error => {res.status(500).json({error})});
};

exports.read = (req, res, next) => {
    var populateQuery = [
        {path:'id_user', model: 'User'},
        {path:'id_ins_aca', model: 'InscriptionAcademique'},
        {path:'id_niv', select:'nom', model: 'Niveau'},
        {path:'id_fil', select:'nom', model: 'Filiere'}
    ];

    NumericID.findOne({num_id: req.query.num_id})
        .populate(populateQuery)
        .then( numericID => res.status(200).json(numericID) )
        .catch( error => res.status(404).json({error}) );
};


exports.readAll = (req, res, next) => {
    var populateQuery = [
        {path:'id_user', model: 'User'},
        {path:'id_ins_aca', model: 'InscriptionAcademique'},
        {path:'id_niv', select:'nom', model: 'Niveau'},
        {path:'id_fil', select:'nom', model: 'Filiere'}
    ];

    NumericID.find( {id_etu: req.query.id_user} )
        .populate(populateQuery)
        .then(numericID => {
            res.status(200).json(numericID);
        })
        .catch(error => res.status(400).json({error}));
};