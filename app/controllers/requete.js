const Requete = require('../models/Requete');
const PieceJointe = require('../models/PieceJointe');
const TypeRequete = require('../models/statics/TypeRequete');
const EtatRequete = require('../models/statics/EtatRequete');
const fs = require('fs');

exports.beforeCreate = (req, res, next) => {
    console.log(JSON.stringify(req.body));
    console.log(JSON.stringify(req.files)); 
    var requeteObject = JSON.parse(req.body.requete);
    req.body.requete = requeteObject;
    req.body.requete.pieces_jointes = [];

    req.files.forEach( (file, i) => {
        console.log("\n\n"+JSON.stringify(file)+ " " + i);
        const pieceJointe = new PieceJointe({
            description: requeteObject.files.descriptions[i],
            image_url: `${req.protocol}://${req.get('host')}/images/${file.filename}`
        });
        req.body.requete.pieces_jointes.push(pieceJointe._id);
        console.log("\n\n"+JSON.stringify(pieceJointe));
        pieceJointe.save()
            .then((p_j) => {
            })
            .catch( error => res.status(500).json({ error }) );
    });
    
    console.log(JSON.stringify(req.body));
    next();

};

exports.create = (req, res, next) => {

    const requete = new Requete({
        ...req.body.requete
    });
    requete.save()
        .then((request) => {
            console.log(JSON.stringify(request));
            res.status(201).json({message: 'Requete envoyée !'});
        })
        .catch(error => {res.status(500).json({error})});
};

exports.read = (req, res, next) => {
    Requete.findById(req.params.id)
        .then( requete => res.status(200).json(requete) )
        .catch( error => res.status(404).json({error}) );
};

exports.update = (req, res, next) => {
    Requete.updateOne({_id: req.params.id}, {...req.body, _id: req.params.id})
        .then(() => res.status(200).json({message: 'Requete modifié !'}))
        .catch((error) => res.status(400).json({error}));
};

exports.delete = (req, res, next) => {
    Requete.findByIdAndDelete(req.params.id)
        .then(() => res.status(200).json({message: 'Requete supprimé !'}))
        .catch(error => res.status(400).json({ error }));
    
};

exports.readAll = (req, res, next) => {
    var populateQuery = [
        {path:'id_etu', select:'nom prenom matricule', model: 'Etudiant'},
        {path:'id_fil', select:'nom', model: 'Filiere'},
        {path:'id_niv', select:'nom', model: 'Niveau'},
        {path:'id_typ_req', select:'nom destinataire etats', model: 'TypeRequete'},
        {path:'id_eta_req', select:'nom', model: 'EtatRequete'},
        {path:'pieces_jointes', model: 'PieceJointe'},
    ];

    console.log( req.query );
    Requete.find( {} )
        .populate(populateQuery)
        .then(requetes => {
            res.status(200).json(requetes);
        })
        .catch(error => res.status(400).json({error}));
};

exports.readAllTypeRequete = (req, res, next) => {

    TypeRequete.find()
        .then(type_requetes => res.status(200).json(type_requetes))
        .catch(error => res.status(400).json({error}));
};