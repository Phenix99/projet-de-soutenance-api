const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

const PaysSchema = mongoose.Schema({
    nom: {type: String, required: true, unique: true}
}); 

PaysSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Pays', PaysSchema);