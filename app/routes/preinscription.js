const express = require('express');
const router = express.Router();

const preinscriptionCtrl = require('../controllers/preinscription')

router.post('/search', preinscriptionCtrl.search);
router.post('/filter', preinscriptionCtrl.filter);
router.post('/paginate', preinscriptionCtrl.paginate);

router.get('/', preinscriptionCtrl.readAll);

router.get('/:id', preinscriptionCtrl.read);

router.post('/', preinscriptionCtrl.beforeCreate, preinscriptionCtrl.create);

router.put('/:id', preinscriptionCtrl.update);

router.delete('/:id', preinscriptionCtrl.delete);

module.exports = router

