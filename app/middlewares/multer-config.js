const multer = require('multer');
var fs = require('fs');

const MIME_TYPES = {
    'image/jpg': 'jpg',
    'image/jpeg': 'jpg',
    'image/png': 'png',
}

const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        var dir = 'public/images';
        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir);
        }
        callback(null, 'public/images');
    },
    filename: (req, file, callback) => {
        console.log("START FILE");
        const name = file.originalname.split(' ').join('_');
        //const extension = file.mimetype.split('/')[file.mimetype.split('/').length-1];
        const uniquePrefix = Date.now() + '_' + Math.round(Math.random() * 1E9) + '_';
        const final_name = uniquePrefix + name /*+ '.' + extension*/;
        console.log("MULTER "+final_name);
        callback(null, final_name);
    }
});

module.exports = multer({ storage });