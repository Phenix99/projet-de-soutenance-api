const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const uniqueValidator = require('mongoose-unique-validator');

const recuSchema = mongoose.Schema({
    id_ins_aca: {type: Schema.Types.ObjectId, ref: 'InscriptionAcademique', required: true},
    numero: {type: String, required: true, unique: true},
    emetteur: {type: String, ref: 'Banque', required: true}
}); 

recuSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Recu', recuSchema);