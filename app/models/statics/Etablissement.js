const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

const etablissementSchema = mongoose.Schema({
    nom: {type: String, required: true, unique: true}
}); 

etablissementSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Etablissement', etablissementSchema);