const express = require('express');
const router = express.Router();

const etudiantCtrl = require('../controllers/etudiant')

router.post('/', etudiantCtrl.create);

router.put('/:id', etudiantCtrl.update);

router.delete('/:id', etudiantCtrl.delete);

router.get('/:id', etudiantCtrl.read);

router.use('/', etudiantCtrl.readAll);

module.exports = router

