const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const uniqueValidator = require('mongoose-unique-validator');

const ueSchema = mongoose.Schema({
    code: {type: String, required: true, unique: true},
    nom: {type: String, required: true, unique: true},
    credit: {type: String, required: true},
    type: {type: String, required: false},
    id_fil: {type: Schema.Types.ObjectId, ref: 'Filiere', required: true},
    id_niv: {type: Schema.Types.ObjectId, ref: 'Niveau', required: true}
}); 

ueSchema.plugin(uniqueValidator);

module.exports = mongoose.model('UE', ueSchema);