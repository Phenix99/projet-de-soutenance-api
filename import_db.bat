mongoimport --db=uy1 --collection=annees --file=db/annees.json
mongoimport --db=uy1 --collection=banques --file=db/banques.json
mongoimport --db=uy1 --collection=departements --file=db/departements.json
mongoimport --db=uy1 --collection=filieres --file=db/filieres.json
mongoimport --db=uy1 --collection=inscriptionacademiques --file=db/inscriptionacademiques.json
mongoimport --db=uy1 --collection=niveaus --file=db/niveaus.json
mongoimport --db=uy1 --collection=preinscriptions --file=db/preinscriptions.json
mongoimport --db=uy1 --collection=recus --file=db/recus.json
mongoimport --db=uy1 --collection=requetes --file=db/requetes.json
mongoimport --db=uy1 --collection=ues --file=db/ues.json
mongoimport --db=uy1 --collection=users --file=db/users.json