const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

const departementSchema = mongoose.Schema({
    nom: {type: String, required: true, unique: true}
}); 

departementSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Departement', departementSchema);