const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

const niveauSchema = mongoose.Schema({
    nom: {type: String, required: true, unique: true}
}); 

niveauSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Niveau', niveauSchema);