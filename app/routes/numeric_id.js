const express = require('express');
const router = express.Router();

const numericIDCtrl = require('../controllers/numeric_id');

router.post('/', numericIDCtrl.create);

router.get('/', numericIDCtrl.read);

router.use('/', numericIDCtrl.readAll);

module.exports = router