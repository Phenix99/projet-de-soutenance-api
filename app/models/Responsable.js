const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');
const User = require('./User');

const responsableSchema = mongoose.Schema({
    poste: {type: String, required: false},
    validated: {type: Boolean, required: true},
});

responsableSchema.plugin(uniqueValidator);

module.exports = User.discriminator('Responsable', responsableSchema);