const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');

const etatRequeteSchema = mongoose.Schema({
    nom: {type: String, required: true},
});

module.exports = mongoose.model('EtatRequete', etatRequeteSchema);