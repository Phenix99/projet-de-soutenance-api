const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

const filiereSchema = mongoose.Schema({
    nom: {type: String, required: true, unique: true}
}); 

filiereSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Filiere', filiereSchema);