/* eslint-disable no-unused-vars */
const User = require('../models/User');
const Responsable = require('../models/Responsable');
const Administrateur = require('../models/Administrateur');
const Etudiant = require('../models/Etudiant');
const bcrypt = require('bcrypt');

const jwt = require('jsonwebtoken');

exports.login = (req, res, next) => {

    User.findOne({email: req.body.email})
        .then( user => {
            if(!user){
                res.status(401).json({message: 'Utilisateur non trouvé !'});
            }
            bcrypt.compare(req.body.password, user.password)
                .then( valid => {
                    if(!valid){
                        res.status(401).json({message: 'Mot de passe incorrect !'});
                    }
                    res.status(200).json({
                        userId: user._id,
                        userRole: user.role,
                        token: jwt.sign(
                            {userId: user._id, userRole: user.role},
                            'RANDOM_TOKEN_SECRET',
                            {expiresIn: '24h'}
                        ) 
                    });
                } )
                .catch( error => res.status(500).json({ error }) );
        })
        .catch( error => res.status(500).json({ error }) );
};


exports.signup = (req, res, next) => {
    let UserModel;
    switch (req.body.role) {
        case 'Responsable':
            UserModel = Responsable;
            break;
        case 'Administrateur':
            UserModel = Administrateur;
            break;
        default:
            res.status(500).json({ error: 'Internal Error !' });
            break;
    }
    bcrypt.hash(req.body.password, 10)
        .then( hash => {
            console.log('Tentative presque réuissie !');
            const user = new UserModel({
                ...req.body,
                password: hash
            });
            user.save()
                .then( () => res.status(201).json( {message: 'Utilisateur enregistré !'} ) )
                .catch( error =>console.log(error) );
        })
        .catch( error => res.status(500).json({ error }) );
};

exports.signupStudent = (req, res, next) => {

    bcrypt.hash(req.body.password, 10)
        .then( hash => {
            User.findOne({matricule: req.body.matricule})
                .then( user => {
                    if(!user){
                        //Si l'Etudiant n'existe pas encore
                        const user = new Etudiant({
                            ...req.body,
                            lieu_nais: "",
                            date_nais: "",
                            password: hash
                        });
                        user.save()
                            .then( () => res.status(201).json( {message: 'Utilisateur enregistré !'} ) )
                            .catch( error =>console.log(error) );
                    }else{
                        //Si l'Etudiant existe déjà ou a déjà fait une inscription académique
                        if( user.password === '' ) {
                            User.updateOne({_id: user._id}, {...req.body,  password: hash, _id: user._id })
                                .then( () => res.status(201).json( {message: 'Etudiant mise à jour !'} ) )
                                .catch( error =>res.status(500).json({ error }) );
                        }else {
                            res.status(401).json( {message: 'Incription non autorisée!'} );
                        }
                       
                    }
                })
                .catch( error =>res.status(500).json({ error }) );
        })
        .catch( error => res.status(500).json({ error }) );
};

exports.read = (req, res, next) => {
    User.findById(req.params.id)
        .then( user => {
            res.status(200).json({ user })
        })
        .catch( error => res.status(404).json({ error }) );
}
