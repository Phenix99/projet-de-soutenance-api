const Preinscription = require('../models/Preinscription');
const User = require('../models/User');
const fs = require('fs');


exports.beforeCreate = (req, res, next) => {
    User.findOne({_id: req.body.student._id})
        .then( user => {
            if(!user){
                res.status(400).json({ error: "Utilisateur non trouvé" })
            }else{
                console.log(JSON.stringify(req.body));
                var options = {upsert: true, new: true,};
                User.findOneAndUpdate({ _id: user._id }, { ...req.body.student, _id: user._id }, options)
                    .exec()
                    .then( (result) => {
                        console.log('Update Handle')
                        req.body.student = result;
                        req.body.id_etu = result._id;
                        next();
                    } )
                    .catch( error => res.status(400).json({ error }) );
            }
        })
        .catch( error =>res.status(500).json({ error }) );
};

exports.create = (req, res, next) => {
    console.log(JSON.stringify(req.body));

    const preinscription = new Preinscription({
        ...req.body
    });
    
    preinscription.save()
        .then(() => {res.status(201).json({message: 'Preinscription réussi !'})})
        .catch(error => {res.status(400).json({error})});
};

exports.read = (req, res, next) => {
    Preinscription.findById(req.params.id)
        .then( preinscription => res.status(200).json(preinscription) )
        .catch( error => res.status(404).json({error}) );
};

exports.update = (req, res, next) => {
    Preinscription.updateOne({_id: req.params.id}, {...req.body, _id: req.params.id})
        .then(() => res.status(200).json({message: 'Preinscription modifié !'}))
        .catch((error) => res.status(400).json({error}));
};

exports.delete = (req, res, next) => {
    Preinscription.findByIdAndDelete(req.params.id)
        .then(() => res.status(200).json({message: 'Preinscription supprimé !'}))
        .catch(error => res.status(400).json({ error }));
    
};

exports.readAll = (req, res, next) => {
    var populateQuery = [
        {path:'id_etu', model: 'Etudiant'},
        {path:'id_fil', select:'nom', model: 'Filiere'},
        {path:'id_niv', select:'nom', model: 'Niveau'},
        {path:'id_dep', select:'nom', model: 'Departement'}
    ];
    Preinscription.find()
        .populate(populateQuery)
        .then(preinscriptions => res.status(200).json(preinscriptions))
        .catch(error => res.status(400).json({error}));
};

exports.search = (req, res, next) => {

    var populateQuery = [
        {path:'id_etu', model: 'Etudiant'},
        {path:'id_fil', select:'nom', model: 'Filiere'},
        {path:'id_niv', select:'nom', model: 'Niveau'},
        {path:'id_dep', select:'nom', model: 'Departement'}
    ];

    console.log({...req.body})
    Preinscription.findOne({...req.body})
        .populate(populateQuery)
        .exec(function(err, preinscriptions) {
            res.status(200).json({ preinscriptions })
        })
};

exports.filter = (req, res, next) => {
    var populateQuery = [
        {path:'id_etu', model: 'Etudiant'},
        {path:'id_fil', select:'nom', model: 'Filiere'},
        {path:'id_niv', select:'nom', model: 'Niveau'},
        {path:'id_dep', select:'nom', model: 'Departement'}
    ];
    
    console.log({...req.body})
    Preinscription.find({...req.body})
        .populate(populateQuery)
        .then(preinscriptions => res.status(200).json(preinscriptions))
        .catch(error => res.status(400).json({error}));
}

exports.paginate = (req, res, next) => {
    const pageOptions = {
        page: parseInt(req.query.page, 10) || 0,
        limit: parseInt(req.query.limit, 10) || 10
    };
    
    Preinscription.find()
        .skip(pageOptions.page * pageOptions.limit)
        .limit(pageOptions.limit)
        .exec(function (err, preinscriptions) {
            if(err) { 
                res.status(500).json(err); 
                return; 
            };
            res.status(200).json(preinscriptions);
        });
}