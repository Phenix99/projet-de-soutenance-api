const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

const RegionSchema = mongoose.Schema({
    nom: {type: String, required: true, unique: true}
}); 

RegionSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Region', RegionSchema);