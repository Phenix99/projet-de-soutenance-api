const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

const banqueSchema = mongoose.Schema({
    nom: {type: String, required: true, unique: true}
}); 

banqueSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Banque', banqueSchema);