const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');
var options = { discriminatorKey: 'role' };

const userSchema = mongoose.Schema({
    email: {type: String, required: true, unique: true},
    password: {type: String, required: false},
    nom: {type: String, required: true},
    prenom: {type: String, required: false},
    sexe: {type: String, required: false},
    numero_tel: {type: String, required: false},
    code_post: {type: String, required: false},
    langue: {type: String, required: false},
    date_ajout: {type: Date, default: Date.now}
}, options);

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User', userSchema);