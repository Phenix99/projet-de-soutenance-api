const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');

const typeRequeteSchema = mongoose.Schema({
    nom: {type: String, required: true},
    objet: {type: String, required: true},
    destinataire: {type: String, required: true},
    pieces: {type: String, required: false},
    icon: {type: String, required: true, default: 'fa fa-question-circle-o icon'},
    etats: [{type: Schema.Types.ObjectId, ref: 'EtatRequete', required: true}],
    description: {type: String, required: true}
});

typeRequeteSchema.plugin(uniqueValidator);

module.exports = mongoose.model('TypeRequete', typeRequeteSchema);