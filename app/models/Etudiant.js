const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');
const User = require('./User');

const etudiantSchema = mongoose.Schema({
    matricule: {type: String, required: true, unique: true},
    nationalite: {type: String, default: 'CAMEROUNAISE'},
    date_nais: {type: String, required: false},
    lieu_nais: {type: String, required: false},
    region: {type: String, required: false} 
}); 

etudiantSchema.plugin(uniqueValidator);

module.exports = User.discriminator('Etudiant', etudiantSchema);